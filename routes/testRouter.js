const express = require('express')
const router = express.Router()

const TestController = require('../controllers/testController')

router.get('/file_cron', TestController.fileCron)
router.get('/ws', TestController.ws)

module.exports = router