const express = require('express');
const router = express.Router();

const mainRouter = require('./mainRouter')
const testRouter = require('./testRouter')

router.use('/', mainRouter)
router.use('/test', testRouter)

module.exports = router