const express = require('express')
const router = express.Router()
const path = require('path')
const multer = require('multer')

const ProfileValidator = require('../validators/ProfileValidator')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const folder = path.dirname(require.main.filename)
        const absolutePath = path.join(folder, 'storage/temp/')
        cb(null, absolutePath)
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
    }
})

const upload = multer({storage: storage})

const asyncHandler = require('express-async-handler')

const AuthController = require('../controllers/authController')
const UserController = require('../controllers/userController')
const ChatController = require('../controllers/chatController')
const authMiddleware = require('../middleware/authMiddleware')

router.post('/singup', AuthController.singUp)
router.post('/login', asyncHandler(AuthController.login))
router.get('/users', [authMiddleware], UserController.getAll)
router.post('/profile', [upload.fields([{name: 'avatar'}, {name: 'banner_photo'}]), ProfileValidator.validate.add()], UserController.profile)

router.get('/chats', [authMiddleware], ChatController.getChats)

module.exports = router
