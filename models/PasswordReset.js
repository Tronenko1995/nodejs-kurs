const sequelize = require('../db')
const {Model, DataTypes} = require('sequelize')

class UserModel extends Model {
}

UserModel.init({
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    token: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'password_resets',
    modelName: 'PasswordReset'
})

module.exports = {
    PasswordReset: PasswordReset
}

