const sequelize = require('../db')
const {Model, DataTypes} = require('sequelize')

class UserModel extends Model {
}

UserModel.init({
    name: {
        type: DataTypes.STRING,
    },
    email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    }
}, {
    defaultScope: {
        attributes: {
            exclude: ['password']
        }
    },
    scopes: {
        withPassword: {
            attributes: {},
        }
    },
    sequelize,
    tableName: 'users',
    modelName: 'UserModel'
})

module.exports = {
    UserModel: UserModel
}

