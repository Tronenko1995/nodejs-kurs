const sequelize = require('../db')
const {Model, DataTypes} = require('sequelize')

class Message extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
        // define association here
    }
}

Message.init({
    sender_id: DataTypes.INTEGER,
    chat_id: DataTypes.INTEGER,
    text: DataTypes.TEXT
}, {
    sequelize,
    modelName: 'Message',
    tableName: 'messages',
});


module.exports = {
    Message
}
