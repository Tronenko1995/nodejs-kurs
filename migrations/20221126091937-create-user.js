'use strict';
const UserRoles = require('../services/UserRoles')
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        const transaction = await queryInterface.sequelize.transaction();
        try {
            await queryInterface.createTable('users', {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                name: {
                    type: Sequelize.STRING
                },
                email: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                password: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    select: false
                },
                role: {
                    type: Sequelize.ENUM(UserRoles.ADMIN, UserRoles.FREELANCER, UserRoles.EMPLOYER),
                    allowNull: false
                },
                description: {
                    type: Sequelize.TEXT
                },
                rate: {
                    type: Sequelize.INTEGER
                },
                position: {
                    type: Sequelize.STRING
                },
                avatar: {
                    type: Sequelize.STRING
                },
                country_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: {
                            tableName: 'countries',
                            schema: 'schema'
                        },
                        key: 'id'
                    },
                },
                level_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: {
                            tableName: 'levels',
                            schema: 'schema'
                        },
                        key: 'id'
                    },
                },
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE
                }
            });
            await queryInterface.addIndex(
                'users',
                {
                    fields: ['email'],
                    unique: true,
                    transaction,
                }
            );
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Users');
    }
};