const CronService = require('../services/CronService')
const path = require('path')

class TestController {
    fileCron = async (req, res) => {
        await CronService.cleanTempDir()

        return res.json('success')
    }

    ws = async (req, res) => {
        // res.render('ws')
        res.sendFile(path.join(__dirname, '../views/ws.html'))
    }
}

module.exports = new TestController()