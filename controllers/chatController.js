const {ChatUser} = require("../models/chatUser");

class ChatController {
    async getChats(req, res) {
        const chats = await ChatUser.findAll({
            where: {
                user_id: req.user.id
            }
        })

        return res.json(chats)
    }
}

module.exports = new ChatController()