const {UserModel} = require("../models/UserModel");
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

class AuthController {
    async singUp(req, res) {
        const saltRounds = 10;
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = bcrypt.hashSync(req.body.password, salt);
        const user = await UserModel.create({
            name: req.body.name,
            email: req.body.email,
            password: hash,
        })
        return res.json(user)
    }

    async login(req, res) {
        const {email, password} = req.body
        // throw new Error()
        let user = await UserModel.scope('withPassword').findOne({
            where: {
                email: email
            }
        })

        if (!user) {
            return res.status(422).json({
                errors: {
                    email: 'Wrong password or email'
                }
            })
        }

        let checkPassword = bcrypt.compareSync(password, user.password)

        if (!checkPassword) {
            return res.status(422).json({
                errors: {
                    email: 'Wrong password or email'
                }
            })
        }

        const token = jwt.sign({id: user.id}, process.env.SECRET_KEY, {
            expiresIn: "7d"
        });

        return res.json({token: token})
    }
}

module.exports = new AuthController()