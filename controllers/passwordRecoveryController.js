const GenerateUniqueToken = require('../services/generateUniqueToken')
const {UserModel} = require("../models/UserModel");

class PasswordRecoveryController {
    passwordForgot = async (req, res) => {

        const user = await UserModel.findOne({
            where: {
                email: req.email
            }
        })

        if (user) {
            let token = GenerateUniqueToken.generateToken()
        }

        return res.json({
            status: 'success'
        })

    }
}

module.exports = new PasswordRecoveryController()