const {Mail} = require("./mail");

class PasswordRecoveryEmail extends Mail {
    async send(to, token) {

        const url = `${proccess.env.FRONT_URL}/password/reset?email=${to}&token=${token}`

        await this.transporter.sendMail({
            from: this.from,
            to: to,
            subject: "Email for password forgot",
            html: `For password recovery go to link <a href="${url}">link</a>`
        })
    }
}

module.exports = {
    PasswordRecoveryEmail: PasswordRecoveryEmail
}