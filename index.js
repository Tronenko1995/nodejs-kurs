const express = require('express')
const app = express()
require('dotenv').config()
const sequelize = require('./db')
const CronKernel = require('./cron/CronKernel')
const {UserModel} = require('./models/UserModel')
const {engine} = require('express-handlebars')
const bodyParser = require('body-parser')
const socket = require("socket.io")
const cors = require('cors')

app.use(cors())
app.use(express.json());
app.use(express.urlencoded());
// in latest body-parser use like below.
app.use(express.urlencoded({extended: true}));
app.engine('handlebars', engine())
app.set('view engine', 'handlebars')
app.set('views', './views')

const logger = require('./logger/logger')

const port = process.env.PORT

const router = require('./routes/index')
const jwt = require("jsonwebtoken");
const {Logform} = require("winston");
const {Message} = require("./models/message");
const {ChatUser} = require("./models/chatUser");


app.use('/', router)

const start = async () => {
    try {
        await sequelize.authenticate()
        console.log('Connection has been established successfully.')
    } catch (error) {
        console.error('Unable to connect to the database:', error)
    }
}

start()
    .then(() => {
        const server = app.listen(port, () => {
            console.log(`Example app listening on port ${port}`)
        })
        // CronKernel()


        const io = socket(server)
        // const nsp = io.of("/users");

        // nsp.on("connection", socket => {
        //     console.log("someone connected");
        //     nsp.emit("hi", "everyone!");
        // });

        io.use((socket, next) => {
            const token = socket.handshake.auth.token;
            console.log(token)
            if (token) {
                try {
                    const decoded = jwt.verify(token, process.env.SECRET_KEY)
                    socket.handshake.auth.id = decoded.id
                    console.log(decoded)
                    return next()
                } catch (error) {
                    const err = new Error('Not authenticated')
                    next(err)
                    // return res.status(401).json({
                    //     status: "error",
                    //     message: "Unauthenticated"
                    // })
                }
            }
            const err = new Error('Not authenticated')
            next(err)
        })


        io.on("connection", (socket) => {
            const userId = socket.handshake.auth.id
            console.log(userId, 'userId')

            socket.join(userId)
            console.log('Connected')

            console.log(socket.handshake.auth.token)

            socket.on('disconect', () => {
                console.log('disconect')
            })

            socket.join("room")
            console.log('Connected')
            socket.emit('observer', {
                message: 'kuku'
            })
            socket.on("otvet", (data) => {
                console.log(data.message)
            })

            socket.on("sendMessage", async (data) => {
                console.log(data.message)

                const message = {
                    sender_id: userId,
                    chat_id: data.chatId,
                    text: data.message
                }

                await Message.create(message)

                const chatUsers = await ChatUser.findAll({
                    where: {
                        chat_id: data.chatId
                    }
                })

                for (const chatUser of chatUsers) {
                    io.to(chatUser.user_id).emit('getMessage', message)
                }


                io.to('room').emit('messages', message)
            })
        })
    })

app.use(function (req, res, next) {
    let error = new Error('Not found')
    error.status = 404
    next(error)
})

if (process.env.DEBUG_MODE === 'true') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500)
        res.json({
            message: err.message || 'Sosamba',
            error: err.stack,
        })
    })
}

app.use(function (err, req, res, next) {
    logger.error(JSON.stringify(`${err.message}${err.stack}`))
    res.status(err.status || 500)
    res.json({
        message: 'Sosamba'
    })
})
