const cron = require('node-cron')
const CronService = require('../services/CronService')
const Module = require("module");

task = cron.schedule('* * * * *', async () => {
    console.log('stopped task')
    await CronService.cleanTempDir()
}, {
    scheduled: false
})

module.exports = task