const cleanTempDir = require('../cron/CleanTempDirCron')

function startCron() {
    cleanTempDir.start()
}

module.exports = startCron