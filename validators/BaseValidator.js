const {validationResult} = require("express-validator");

class BaseValidator {

    constructor() {
        this.reporter = (req, res, next) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                let validationErrors = {}
                for (const error of errors.array()) {
                    validationErrors[error.param] = error.msg
                }
                return res.status(422).json({errors: validationErrors});
            }

            next()
        }

        this.add = () => {
            return [
                this.createValidators(),
                this.reporter
            ]
        }
    }

    createValidators = () => []
}

module.exports = {
    validator: BaseValidator
}