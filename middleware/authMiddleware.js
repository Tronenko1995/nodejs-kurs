const jwt = require("jsonwebtoken")

module.exports = (req, res, next) => {
    let token = req.headers.authorization?.split(' ')[1]
    if (token) {
        try {
            req.user = jwt.verify(token, process.env.SECRET_KEY)
            return next()
        } catch (err) {
            return res.status(401).json({
                status: "error",
                message: "Unauthenticated"
            })
        }
    }
    return res.status(401).json({
        status: "error",
        message: "Unauthenticated"
    })
}