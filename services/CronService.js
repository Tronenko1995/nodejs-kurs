const moment = require('moment')

const fs = require('fs')
const path = require('path')
const logger = require('../logger/logger')
const {promisify} = require('util')
const unlinkAsync = promisify(fs.unlink)

class CronService {
    cleanTempDir = async () => {
        const directory = path.join(__dirname, '../storage/temp')
        fs.readdir(directory, async (err, files) => {
            if (err) {
                logger.error(JSON.stringify(`${err.message}${err.stack}`))
            } else {
                files.forEach((file) => {
                    if (file !== '.gitignore') {

                        const filePath = `${directory}/${file}`
                        const {birthtime} = fs.statSync(filePath)
                        const createdDate = moment(birthtime)
                        const starTime = moment()
                        const duration = moment.duration((starTime.diff(createdDate)))
                        const hour = duration.asHours()
                        if (hour > 4) {
                            unlinkAsync(filePath)
                        }

                        console.log(fs.statSync(filePath))


                    }
                })
            }
        })
    }
}

module.exports = new CronService()